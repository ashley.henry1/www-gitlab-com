---
layout: markdown_page
title: "Category Strategy - <CATEGORY-NAME>"
---

- TOC
{:toc}

## PaaS

| | |
| --- | --- |
| Stage | [Configure](/direction/configure/) |
| Maturity | [Minimal](/direction/maturity/) |

### Overview

Platform-as-a-Service (PaaS) is a category of cloud computing services that provides a platform allowing customers to develop, run, and manage applications without the complexity of building and maintaining the infrastructure typically associated with developing and launching an app.

The advantages of PaaS are primarily that it allows for higher-level programming with dramatically reduced complexity; the overall development of the application can be more effective, as it has built-in/self up-and-down ramping infrastructure resources; and maintenance and enhancement of the application is thus easier.

An important point on our [pricing strategy](https://about.gitlab.com/strategy/#pricing):

> We charge for making people more effective and will charge per user, per application, or per instance. We do include free minutes with our subscriptions and trials to make it easier for users to get started. As we look towards more deployment-related functionality on .com it's tempting to offer compute and charge a percent on top of, for example, Google Cloud Platform (GCP). We don't want to charge an ambiguous margin on top of another provider since this limits user choice and is not transparent. So we will always let you BYOK (bring your own Kubernetes) and never lock you into our infrastructure to charge you an opaque premium on those costs.


### Where we are Headed

**Remove the complexity of provisioning and maintaining the infrastructure necessary for developing and launching an application.**

These complexity and concerns stem mostly from scale, cost, security, and scalability. Configurability and customization are also important and as Kubernetes and Knative mature, more options will be available for operators to alleviate these concerns.

We want companies of all sizes to be able to setup an efficient, scalable, and robust application development platform. Eventually we want to bring this to gitlab.com and maybe even provide limited free compute to showcase the full power of GitLab to users.

From our original [DevOps strategy post](https://about.gitlab.com/2017/10/04/devops-strategy/):

```
[...]if we want everybody to have CI, well, let’s turn it on for you. That’s pretty awesome. If we want everybody to have CD, we can’t just turn it on for you, because you have to have a place to deploy it to. So, if we just provided you a Kubernetes cluster ("everybody gets a cluster"), then you just got a place. And, I mean, we’ll severely limit it. We’ll make it limited in some way, so that you’re not going to run the production stuff for long there. Or if you do, you have to pay for it. But we’re not going to try and make money off of the production resources. We want to make money off of making it really easy.
```

### Considerations

- Must ensure security for prod ready applications
- Must avoid use of privileged docker images, no root use
- Use kaniko to build containers and avoid docker-in-docker
- Resources could idle when not in use for max efficiency

### Target Audience and Experience

* [Development team lead](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
* [Software developer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
* [Devops engineer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
* [Systems administrator](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)
* 

### What's Next & Why

[Operator can see all deployments made to a group/instance level cluster](https://gitlab.com/gitlab-org/gitlab-ce/issues/630990)

In order to take advantage of the existing features of GitLab that can be used to build a PaaS (instance/group level clusters, Auto DevOps), we want to provide necessary operator support in the the form of observability and logging. Starting with deployment visibility will allow operators to gauge use and plan for capacity.

GitLab Serverless will soon support installation of [Knative at the group/instance level](https://gitlab.com/gitlab-org/gitlab-ce/issues/62666) which will allow us to further the PaaS offering with applications that automatically scale down to zero.

### What is Not Planned Right Now

While we would like to provide some form of free compute space for GitLab.com, this is not planned for the short term.

### Maturity Plan

This category is currently at the `Planned` maturity level, and our next maturity target is `Minimal` (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

- https://gitlab.com/groups/gitlab-org/-/epics/111

### Competitive Landscape

### Heroku
Heroku is a cloud platform as a service supporting several programming languages. Heroku, one of the first cloud platforms, has been in development since June 2007, when it supported only the Ruby programming language, but now supports Java, Node.js, Scala, Clojure, Python, PHP, and Go.

### Cloud Foundry
Cloud Foundry is an open source, multi-cloud application platform as a service governed by the Cloud Foundry Foundation, a 501 organization. The software was originally developed by VMware and then transferred to Pivotal Software, a joint venture by EMC, VMware and General Electric. Its hosted service is called Pivotal Web Services, it bills itself as a modern runtime for Spring Boot, .NET, and Node apps.

### Google App Engine
Google App Engine is a web framework and cloud computing platform for developing and hosting web applications in Google-managed data centers. Applications are sandboxed and run across multiple servers.

### Analyst Landscape

From `The Key Trends in PaaS and Platform Architecture, 2019` (Gartner)

From 2018 through 2022, the PaaS market is projected to double in size, and nearly half of all PaaS offerings in 2019 will be public cloud only. Customers turn to cloud platforms for increasingly strategic business initiatives.

Application leaders developing cloud computing strategies must:

* Support business access to modern technology by strategic investment in cloud platforms. Many PaaS offerings have reached enterprise-level maturity, some platform innovation is cloud-
only.
* Support continuous innovation by embracing API- and event-driven computing. Encourage the
assembly of applications and incremental design to fight the “not invented here” syndrome.
* Take full advantage of cloud investments by engaging the platform role of all cloud services,
including IaaS, PaaS and SaaS.
* Optimize the use of computing resources by distributing computing investments between public
cloud, Internet of Things (IoT) edge, and managed on-premises computing.
* Champion IT-business collaboration and promote organizational/cultural policies that support
agility and innovation in IT, in order to reduce barriers to agility and innovation in business.

### Top Customer Success/Sales issue(s)

Specify Auto DevOps deployment target https://gitlab.com/gitlab-org/gitlab-ce/issues/58998

### Top user issue(s)

TBD

### Top internal customer issue(s)

TBD

### Top Strategy Item(s)
[Operator can see all deployments made to a group/instance level cluster](https://gitlab.com/gitlab-org/gitlab-ce/issues/630990)