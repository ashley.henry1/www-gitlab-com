---
layout: markdown_page
title: "Content schedule"
---

This list is an outline of the highest priority content scheduled. Content on this list either aligns to a content pillar, corporate marketing event, and/or brand initiative. Things on this list can change and there are things we produce that are not on this list. This is our best estimate of what to expect, when. 

Real-time updates to the blog calendar can be found on the [blog handbook page](/handbook/marketing/blog/#blog-calendar).

## FY20-Q2 (2019/05 - 2019/07)

### Pillars 

- [2019 Developer report](https://gitlab.com/groups/gitlab-com/marketing/-/epics/11) <kbd>Editorial</kbd>
- [Reduce cycle time](https://gitlab.com/groups/gitlab-com/marketing/-/epics/47) <kbd>Dev</kbd>
- [Application modernization](https://gitlab.com/groups/gitlab-com/marketing/-/epics/49) <kbd>Ops</kbd>
- [Secure apps](https://gitlab.com/groups/gitlab-com/marketing/-/epics/48) <kbd>Security</kbd>

### June

- [Agile mindset](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/640) <kbd>Dev</kbd>
- [Agile technical practices](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/639) <kbd>Dev</kbd>
- [Scoped labels technical post](https://gitlab.com/gitlab-com/www-gitlab-com/issues/4256) <kbd>Dev</kbd>
- Scoped labels GIF <kbd>Social</kbd>
- [How GitLab uses GitLab for agile delivery](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/643) <kbd>Dev</kbd>
- [Modernize CI/CD blog post 1](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/588) <kbd>Ops</kbd>
- [Modernize CI/CD blog post 2](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/589) <kbd>Ops</kbd>
- [Auto-scaling runners technical blog post](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/615) <kbd>Ops</kbd>
- Auto-scaling runners GIF <kbd>Social</kbd>
- [The risk of third party code](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/624) <kbd>Security</kbd>
- [What is SAST and DAST?](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/625) <kbd>Security</kbd>
- [Compliance as code](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/629) <kbd>Security</kbd>

### July

- [How to manage multiple agile teams with microservices](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/644) <kbd>Dev</kbd>
- [VSM: A snapshot of your lifecycle](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3779) <kbd>Dev</kbd>
- [Delays in the development process](https://gitlab.com/gitlab-com/www-gitlab-com/issues/3817) <kbd>Dev</kbd>
- [Agile for developers: incremental code development](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/641) <kbd>Dev</kbd>
- [Agile best practice](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/642) <kbd>Dev</kbd>
- [GitLab's agile mindset](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/646) <kbd>Dev</kbd>
- [Modernize CI/CD blog post 3](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/590) <kbd>Ops</kbd>
- [Modernize CI/CD blog post 4](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/591) <kbd>Ops</kbd>
- Dev Technical post 3 <kbd>Dev</kbd>
- Dev Technical post 4 <kbd>Dev</kbd>
- Ops Technical post 3 <kbd>Ops</kbd>
- Ops Technical post 4 <kbd>Ops</kbd>
- [How to reduce technical debt with dependency scanning and auto-remediation](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/628) <kbd>Security</kbd>
- [What is container based security?](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/666) <kbd>Security</kbd>
- [Security testing principles every developer should know](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/626)
- [CI/CD topic web page](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/616) <kbd>Ops</kbd>
- [2019 Developer Report](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/395) <kbd>Dev</kbd> 
- [Modernize CI/CD eBook](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/422) <kbd>Ops</kbd>
- [Developer's guide to application security](https://gitlab.com/gitlab-com/marketing/corporate-marketing/issues/623) <kbd>Security</kbd>


## FY20-Q3 (2019/08 - 2019/10)

### Pillars

- 2019 Developer report <kbd>Editorial</kbd>
- Toolchain complexity <kbd>Dev</kbd>
- Future of cloud <kbd>Ops</kbd>
- Zero trust <kbd>Security</kbd>

### August



### September



### October



## FY20-Q4 (2019/11 - 2020-01)

- 2020 Developer survey <kbd>Editorial</kbd>
- Toolchain complexity <kbd>Dev</kbd>
- Future of cloud <kbd>Ops</kbd>
- Zero trust <kbd>Security</kbd>

### November



### December


### January

## FY21-Q1 (2020/02 - 2020-04)

- 2020 Developer Report <kbd>Editorial</kbd>
- TBD <kbd>Dev</kbd>
- TBD <kbd>Ops</kbd>
- Cloud security <kbd>Security</kbd>

### February



### March


### April

