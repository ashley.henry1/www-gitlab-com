---
layout: markdown_page
title: "Integrate with GitLab"
description: Learn about integrating with GitLab, as well as partnership, marketing, and licensing opportunities.
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Open to collaboration

GitLab is an open product with a Community Edition product that is fully open-source and an Enterprise Edition product that is built on our Community Edition but with additional features that are closed-sourced. We're open to integrations with companies and tools that are interested in harnessing GitLab's platform capabilities, user flows and data with their products. If you want to talk to us about a partnership you can contact [Mayank Tahilramani](mailto:mayanktahil@gitlab.com), Sr. Alliance Manager.

## Integrating with GitLab

### Ways to integrate

If you want to integrate with GitLab there are three possible paths you can take:

1. Utilizing webhooks - If you want to reflect information from GitLab or initiate an action based on a specific activity that occurred on GitLab you can utilize the existing infrastructure of our webhooks. To read about setting up webhooks on GitLab visit this [page](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/web_hooks/web_hooks.md).

2. API integration - If you're seeking a richer integration with GitLab, the next best option is to utilize our ever expanding [API](https://docs.gitlab.com/ee/api/). The API contains pretty much anything you'll need, however if there's an API call that is missing we'd be happy to explore it and develop it.

3. Project Services - Project services give you the option to build your product into GitLab itself. This is the most advanced and complex method of integration but is by far the richest. It requires you to add your integration to GitLab's code base as a standard contribution. You can see the list of project services [available](http://docs.gitlab.com/ee/project_services/project_services.html), and look into this [example](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/4930/diffs). If you're thinking of creating a project service, the steps to follow are similar to any [contribution](https://gitlab.com/gitlab-org/gitlab-test/blob/master/CONTRIBUTING.md) to the GitLab codebase.

### Getting your app listed

If you've completed an integration you can have your integration listed on our [technology partners page](/partners/). To be listed you'll need to submit a Merge Request (MR) with:

  1. A URL page with details on the integration along with screenshots as well as reference to the technical documentation and the steps required to set it up.
  2. A short description of the integration (up to 314 characters)
  3. Listed name for the integration on the applications page
  4. Your application's logo (optional)

Once you have those ready, go ahead and create a [merge request (MR) on this repository](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests)
following the steps described below. From the dropbox menu right beside the MR title,
select the MR description template called "Applications". Once you're ready, mention
`@mayanktahil` in the MR for review.

#### Applications YAML file

To add your app to the [technology partners page](/partners/), you need to add your app's data
to the file [`data/applications.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/applications.yml)
using the following code block:

```yaml
- title: 'App name'
  content: 'App description.'
  links:
    - url: 'App URL'
      title: 'Display text for the URL'
```

If necessary, you can add up to 3 links:

```yaml
- title: 'App name'
  content: 'App description.'
  links:
    - url: 'App URL'
      title: 'Display text for the URL' # example: your app/website URL
    - url: 'A second URL'
      title: 'Display text for the second URL' # example: a blog post describing how the integration works
    - url: 'A third URL'
      title: 'Display text for the third URL' # example: your app's repository URL
```

Choose the app's category accordingly. The code block is to be added
to **the end** of the category list it belongs to.

Notes:

- Write in sentence case, using capital letters for the beginning of the sentences,
product names, method names, and feature names.
- Always write GitLab with a capital G and L.
- Respect the YAML file's indentation.
- Avoid special characters, particularly colons and quotes, otherwise they may need
to be manually escaped to not break the YAML syntax.

#### Application logo

To add your app's logo, name the file after the application name that you added to the `title`,
using sentence case and replacing any blank spaces with underscores `_`. E.g., if your app's
name is "Hello World", your logo should be named `hello_world.png`. The extensions
supported are `.png` and `.jpg`, and the max image size is 70KB.

Add your logo to the [`source/images/applications/apps/`](https://gitlab.com/gitlab-com/www-gitlab-com/tree/master/source/images/applications/apps) directory, into the same branch
used for the YAML file changes.

Note that adding your app's logo is optional, but highly recommended.

### Assistance from GitLab

We're always here to help you through your efforts of integration. If there's a missing API call from our current API, or you ran into other difficulties in your development please feel free to create a new issue on the Community Edition [issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues) and apply the `Ecosystem` label.

## Resale Integration Partnerships

We're always looking to explore partnering with complementary integrations that are suited for large teams of developers. If you interested in exploring a partnership that fits that bill please contact [Mayank Tahilramani](mailto:mayanktahil@gitlab.com).

## Co-Marketing

We love spreading the word on new integrations for GitLab to our community and followers. We're open to pursuing any of the following for co-marketing:

  1. Social channels mentions and posts
  2. Guest blog post or a dedicated blog post
  3. Mention in our newsletter
  4. Webinar on the partnership and the integration by both parties
  5. Customer story about the joint benefits from the integration
  6. Events and conferences

Contact [Erica Lindberg](mailto:erica@gitlab.com), Content Marketing Manager, to discuss co-marketing opportunities.

## EE dev license

If you're working on an EE integration and need a GitLab EE license to develop and test on we will provide that upon request.

## Talk to us

If you want to talk to us about a partnership you can contact [Mayank Tahilramani](mailto:mayanktahil@gitlab.com).
